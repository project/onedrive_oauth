<?php

/**
 * @file
 * Administration page callbacks for the OneDrive OAuth module.
 */

/**
 * Used to configure the OneDrive App settings.
 */
function onedrive_oauth_api_keys_settings($form, &$form_state) {
  $form['onedrive_oauth_appid'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Application ID'),
    '#default_value' => variable_get('onedrive_oauth_appid', NULL),
    '#description' => t('Also called the <em>OAuth client_id</em> value on <a href="https://account.live.com/developers/applications/index">OneDrive App</a> settings pages.'),
  );

  $form['onedrive_oauth_skey'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Application Secret'),
    '#default_value' => variable_get('onedrive_oauth_skey', NULL),
    '#description' => t('Also called the <em>OAuth client_secret</em> value on OneDrive App settings pages.'),
  );

  $form['onedrive_oauth_connect_url'] = array(
    '#type' => 'textfield',
    '#attributes' => array('readonly' => 'readonly'),
    '#title' => t('Connect url'),
    '#description' => t('Copy this value into OneDrive Applications settings page'),
    '#default_value' => $GLOBALS['base_url'] . '/onedrive-oauth',
  );

  $form['onedrive_oauth_login_only'] = array(
    '#type' => 'checkbox',
    '#attributes' => array(),
    '#title' => t('Login Only (No Registration)'),
    '#description' => t('Allow only existing users to login with OneDrive. New users can not signup using OneDrive OAuth.'),
    '#default_value' => variable_get('onedrive_oauth_login_only', 1),
  );

  $form['onedrive_oauth_post_login_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Post Login url'),
    '#description' => t('Drupal URL to which the user should be redirected to after successful login.'),
    '#default_value' => variable_get('onedrive_oauth_post_login_url', ''),
  );

  return system_settings_form($form);
}
