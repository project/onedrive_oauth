
Welcome to OneDrive OAuth for Drupal

Usage:
------
This Module can be used for Single Sign On with OneDrive for already registered
users.

Installation:
-------------
Install as any other module: 
http://drupal.org/documentation/install/modules-themes

This module does not require any third-party library to work with
authentication.

App creation/Configure:
-----------------------
1) Go to admin/config/people and "OneDrive OAuth Settings" link
2) Copy "Connect url" in notepad

OneDrive App creation:
----------------------
1) Go to https://account.live.com/developers/applications/index
2) click on "Create application"
3) Provide the name for your application
4) Go To "API Settings" tab
5) Enter the Redirect url from "Connect url"
6) Go to "APP Settings" tab
7) Copy "Client ID" value to the drupal OneDrive OAuth settings
"Application ID" textbox
8) Copy "Client secret (v1)" value to the drupal OneDrive OAuth settings
"Application Secret" textbox
9) click save configuration button

How to enable OneDrive Single Sign On
-------------------------------------
1) For the first time the user needs to login to their drupal site with their
drupal login credentials.
2) Navigate to "My Account" Page -> "OneDrive OAuth map"
3) Click the link and its automatically Navigate to the OneDrive Login Page
Give the Permission for the OneDrive APP

Now you are ready for single sign on with OneDrive
